//
// vm.c
//
// Stubs for the visible functions at the core of the maTe VM.
//

#include <string.h>

#include "vm.h"

/*
 * Global pointer to class memory.
 */
long memory_size;
uint32_t *virtual_memory;

/*
 * Global pointer to statck memory.
 */
const long stack_size = 4096;
void **stack_memory;


/////////////////////////////////////////////////////////////////////
// initialization and termination functions

// function to initialize VM given a class file name
// note: if there is a problem during initialization, this function will
//       terminate the program.
// note: all following functions should not be called until this function
//       returns.
void initializeVM(unsigned int heapSize, char *classFileName)
{
    // Get the length of the file by jumping to the end and getting the offset
    FILE *file = fopen(classFileName, "rb");
    if (file == NULL) {
        fprintf(stderr, "Could not open class file: %m\n");
        exit(EXIT_FAILURE);
    }
    fseek(file, 0, SEEK_END);
    long length = ftell(file);
    fseek(file, 0, SEEK_SET);

#if P_DEBUG
        fprintf(stderr, "Reading %ld bytes from %s\n", length, classFileName);
#endif

    // Make sure can divide the file into 4-byte words
    if (length % 4 != 0) {
        fprintf(stderr, "Could not divide class file into 4-byte words\n");
        exit(EXIT_FAILURE);
    }

    // Create a buffer to put the data into
    virtual_memory = (uint32_t *)malloc(length);
    memory_size = length;
    if (virtual_memory == NULL) {
        fprintf(stderr, "Could not allocate virtual memory: %m\n");
        exit(EXIT_FAILURE);
    }
    memset(virtual_memory, 0, length);

    // Read the entire file into the buffer
    fread(virtual_memory, length, 1, file);
    if (feof(file)) {
        fprintf(stderr, "Could not read class file: %m\n");
        exit(EXIT_FAILURE);
    }
    fclose(file);

    // Convert to little-endian
    for (int i = 0; i < length; i += 4) {
        uint8_t *buffer = (uint8_t *)virtual_memory;
        uint8_t first = buffer[i];
        uint8_t second = buffer[i + 1];
        uint8_t third = buffer[i + 2];
        uint8_t fourth = buffer[i + 3];

        buffer[i] = fourth;
        buffer[i + 1] = third;
        buffer[i + 2] = second;
        buffer[i + 3] = first;
    }

    // Allocate the frame stack
    stack_memory = malloc(sizeof(uint32_t *) * stack_size);
}

// function to halt VM with a message
void haltVM(const char *msg, int exitCode)
{
   if (msg != NULL)
   {
     fprintf(stderr, "%s\n", msg);
   }

   // Free the memory.
   free(virtual_memory);
   free(stack_memory);

   exit(exitCode);
}

/////////////////////////////////////////////////////////////////////
// support for Garbage Collector to ask for the roots of the object graph

// ask for the roots of the object graph and give a callBack function
// that will be called for each root. When there are no more roots a
// 0 will be passed to the callBack and then getRoots will return.
void getRoots(void ((*callBack)(Reference)))
{
   fprintf(stderr, "getRoots not implemented!\n");
}

