//
// vm.h
//
// Header file to describe central data structures for the maTe virtual
// machine.
//
// Phil Hatcher

// General rules for this set of functions:
//   1. Bad parameters should generally cause a function to terminate the
//      program (by calling haltVM). Functionality exists for the caller to
//      validate the parameters in advance, if necessary.
//   2. The function, initializeVM, must be called before any other
//      function.
//   3. Explicitly assuming References are represented with a C
//      integer or pointer type, so can manipulate them with C operators,
//      such as ==.
//   4. The VM should assume that the predefined class Object appears first
//      in the class table in a class file, Integer will be second,
//      String will be third, and Table will be fourth.
//

#define DEBUG 1

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/////////////////////////////////////////////////////////////////////
// some general definitions

// type for VM address
typedef uint32_t Address;

// type for field and method index
typedef uint32_t Index;

// type for class descriptor address
typedef uint32_t Class;

// type for number of methods or fields or locals or...
typedef uint32_t Count;

/////////////////////////////////////////////////////////////////////
// initialization and termination functions

// function to initialize VM given a class file name
// note: if there is a problem during initialization, this function will
//       terminate the program.
// note: all following functions should not be called until this function
//       returns.
void initializeVM(unsigned int heapSize, char *classFileName);

// function to halt VM with a message
void haltVM(const char *msg, int exitCode);

/////////////////////////////////////////////////////////////////////
// heap: responsible for allocating and accessing memory for objects

// handle for allocated object, ie a reference
// (need this is to fit in 32 bits, even on a 64-bit machine)
typedef uint32_t Reference;

// function to allocate an object given its class
Reference allocateObject(Class class);

// function to allocate a String object given its initial value
// note: need a separate allocation function because of its variable length
// note: initial value must be copied into the new object, of course
// note: input string is the usual null-terminated character string
Reference allocateString(const char *initialValue);

// function to retrieve class from object given its reference
Class getClass(Reference ref);

// get string value from String object
// note: the return value should not be changed
// note: the return value is the usual null-terminated character string
const char *getStringValue(Reference ref);

// get integer value from Integer object
int getIntegerValue(Reference ref);

// put integer value into Integer object
void putIntegerValue(Reference ref, int value);

// function to retrieve field from object given its reference and
// the field index
Reference getField(Reference ref, Index index);

// function to update field in object given its reference,
// the field index and the reference to store in the field
void putField(Reference objectRef, Index index, Reference newFieldValue);

/////////////////////////////////////////////////////////////////////
// classes: support for accessing classes

// validate a class descriptor
bool isValidClass(Class class);

// returns the class descriptor for the predefined class Object
Class getObjectClass(void);

// returns the class descriptor for the predefined class Integer
Class getIntegerClass(void);

// returns the class descriptor for the predefined class String
Class getStringClass(void);

// returns the class descriptor for the predefined class Table
Class getTableClass(void);

// returns the class descriptor for the i-th class in the class table
Class getIthClass(int i);

// returns true if the given class is the predefined class Object
bool isObjectClass(Class class);

// returns true if the given class is the predefined class Integer
bool isIntegerClass(Class class);

// returns true if the given class is the predefined class String
bool isStringClass(Class class);

// returns true if the given class is the predefined class Table
bool isTableClass(Class class);

// returns the super class for the given class, or zero if there
// is no super class, ie the given class is Object
Class getSuper(Class class);

// returns the number of fields for the given class
Count getNumFields(Class class);

// returns the number of methods for the given class
Count getNumMethods(Class class);

// returns the address of a method given its class and its index
Address getMethodAddress(Class class, Index index);

// returns the number of locals for a method given its class and its index
// note: if the method is a native method then this routine returns the
//       native method index.
Count getMethodNumLocals(Class class, Index index);

// reads word from address inside a method or constructor
// body, or from address inside a main block body. This
// can be used for all pieces of instructions. word is returned
// as an int.
int getWordFromCode(Address addr);

/////////////////////////////////////////////////////////////////////
// support for the main block

// returns the address of the main block
Address getMainBlockAddress(void);

// returns the number of locals for the main block
Count getMainBlockNumLocals(void);

/////////////////////////////////////////////////////////////////////
// support for frames
//   a frame contains a local slot array, a PC, and an operand stack

// handle for frames
typedef void *Frame;

// create a frame given its local slot array length and its initial PC
Frame createFrame(Count localsLength, Address pc);

// delete a frame
void deleteFrame(Frame frame);

// is operand stack in frame empty?
bool isEmptyOperandStack(Frame frame);

// push reference onto operand stack of frame
void pushOperandStack(Frame frame, Reference ref);

// pop reference from operand stack of frame
Reference popOperandStack(Frame frame);

// retrieve the i-th item on the operand stack of frame (0 is the top,
// 1 is one beneath the top, 2 is two beneath the top, etc)
Reference peekOperandStack(Frame frame, Index i);

// return length of local slot array of frame
Count lengthLocals(Frame frame);

// get reference from local slot array of frame given its index
Reference getLocals(Frame frame, Index index);

// put reference into local slot array of frame given its index
void putLocals(Frame frame, Index index, Reference ref);

// get PC from frame
Address getPC(Frame frame);

// put PC into frame
void putPC(Frame frame, Address pc);

/////////////////////////////////////////////////////////////////////
// support for the frame stack
//   note: the frame stack (and there is only one) is created by
//         initializeVM.

// is frame stack empty?
bool isEmptyFrameStack(void);

// push frame onto frame stack
void pushFrame(Frame frame);

// pop frame from frame stack
Frame popFrame(void);

// get top frame from frame stack but do not pop
Frame getTopFrame(void);

/////////////////////////////////////////////////////////////////////
// interface to native code

// invoke a native method given its index
void invokeNativeMethod(Index index);

/////////////////////////////////////////////////////////////////////
// support for Garbage Collector to ask for the roots of the object graph

// ask for the roots of the object graph and give a callBack function
// that will be called for each root. When there are no more roots a
// 0 will be passed to the callBack and then getRoots will return.
void getRoots(void ((*callBack)(Reference)));

