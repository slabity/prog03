//
// native.c
//
// implementation of the native methods for the pre-defined classes
//
// Phil Hatcher
//
// The key to understanding the implementation of the native methods
// is that no VM frame is allocated for them. They are executed directly
// on the native machine stack and must retrieve their parameters from the
// operand stack of the top frame in the VM.
//
// The native methods are implemented without error checking, meaning
// that any problem will result in the program terminating.
//
// The native methods for the Table class are not yet implemented.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "vm.h"

// nothing to do so just pop the reference
static void native_object_constructor(void)
{
  (void) popOperandStack(getTopFrame());
  return;
}

// pop the two references
// test them for equality
// if equal create an Integer object with value 1
// else create an Integer object with value 0
// push its reference
static void native_object_equals(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference ref = popOperandStack(frame);
  Reference this = popOperandStack(frame);

  if (this == ref)
  {
    putIntegerValue(ret, 1);
  }
  else
  {
    putIntegerValue(ret, 0);
  }
  pushOperandStack(frame, ret);
  return;
}

// pop the reference
// cast the reference value to an int
// create an Integer object with that int value
// push its reference
static void native_object_hash_code(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference ref = popOperandStack(frame);
  putIntegerValue(ret, (int) ref);
  pushOperandStack(frame, ret);
  return;  
}

// pop and discard the reference
// create a String object with value "Object"
// push its reference
static void native_object_to_string(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateString("Object");
  (void) popOperandStack(frame);
  pushOperandStack(frame, ret);
  return;  
}

// nothing to do so just pop the reference
static void native_integer_constructor(void)
{
  (void) popOperandStack(getTopFrame());
  return;
}

// pop the old Integer reference
// pop the new Integer reference
// get the value from the old object and store it
// in the new object
static void native_integer_constructor_integer(void)
{
  Frame frame = getTopFrame();
  Reference ref = popOperandStack(frame);
  Reference this = popOperandStack(frame);
  putIntegerValue(this, getIntegerValue(ref));
  return;
}

// pop two references to Integer objects
// retrieve the two values and add them
// put result in new Integer object
// return its reference
static void native_integer_add(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference ref = popOperandStack(frame);
  Reference this = popOperandStack(frame);
  int result = getIntegerValue(this) + getIntegerValue(ref);
  putIntegerValue(ret, result);
  pushOperandStack(frame, ret);
  return;
}

// pop two references to Integer objects
// retrieve the two values and subtract them
// put result in new Integer object
// return its reference
static void native_integer_subtract(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference ref = popOperandStack(frame);
  Reference this = popOperandStack(frame);
  int result = getIntegerValue(this) - getIntegerValue(ref);
  putIntegerValue(ret, result);
  pushOperandStack(frame, ret);
  return;
}

// pop two references to Integer objects
// retrieve the two values and multiply them
// put result in new Integer object
// return its reference
static void native_integer_multiply(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference ref = popOperandStack(frame);
  Reference this = popOperandStack(frame);
  int result = getIntegerValue(this) * getIntegerValue(ref);
  putIntegerValue(ret, result);
  pushOperandStack(frame, ret);
  return;
}

// pop two references to Integer objects
// retrieve the two values
// if non-this value is zero, then halt the VM with error message
// else divide this value by non-this value
// put result in new Integer object
// return its reference
static void native_integer_divide(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference ref = popOperandStack(frame);
  Reference this = popOperandStack(frame);
  if (getIntegerValue(ref) == 0)
  {
    haltVM("divide by zero\n", -1);
  }
  int result = getIntegerValue(this) / getIntegerValue(ref);
  putIntegerValue(ret, result);
  pushOperandStack(frame, ret);
  return;
}

// pop two references to Integer objects
// retrieve the two values and compare them
// if this value is greater than non-this value
// then create new Integer object with value 1
// else create new Integer object with value 0
// return its reference
static void native_integer_greater_than(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference ref = popOperandStack(frame);
  Reference this = popOperandStack(frame);
  if (getIntegerValue(this) > getIntegerValue(ref))
  {
    putIntegerValue(ret, 1);
  }
  else
  {
    putIntegerValue(ret, 0);
  }
  pushOperandStack(frame, ret);
  return;
}

// pop two references to Integer objects
// retrieve the two values and compare them
// if this value is less than non-this value
// then create new Integer object with value 1
// else create new Integer object with value 0
// return its reference
static void native_integer_less_than(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference ref = popOperandStack(frame);
  Reference this = popOperandStack(frame);
  if (getIntegerValue(this) < getIntegerValue(ref))
  {
    putIntegerValue(ret, 1);
  }
  else
  {
    putIntegerValue(ret, 0);
  }
  pushOperandStack(frame, ret);
  return;
}

// pop reference to Integer object
// retrieve the value
// if this value is equal to 0
// then create new Integer object with value 1
// else create new Integer object with value 0
// return its reference
static void native_integer_not(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference this = popOperandStack(frame);
  if (getIntegerValue(this) == 0)
  {
    putIntegerValue(ret, 1);
  }
  else
  {
    putIntegerValue(ret, 0);
  }
  pushOperandStack(frame, ret);
  return;
}

// pop reference to Integer object
// retrieve the value and negate it
// create new Integer object with the result value
// return its reference
static void native_integer_minus(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference this = popOperandStack(frame);
  putIntegerValue(ret, -getIntegerValue(this));
  pushOperandStack(frame, ret);
  return;
}

// pop two references to Integer objects
// retrieve the two values and compare them
// if the values are equal
// then create new Integer object with value 1
// else create new Integer object with value 0
// return its reference
static void native_integer_equals(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference ref = popOperandStack(frame);
  Reference this = popOperandStack(frame);
  if (getIntegerValue(this) == getIntegerValue(ref))
  {
    putIntegerValue(ret, 1);
  }
  else
  {
    putIntegerValue(ret, 0);
  }
  pushOperandStack(frame, ret);
  return;
}

// do nothing. just leave the Integer object
// on top of the stack to be its own hash code
static void native_integer_hash_code(void)
{
  return;
}

// pop reference to Integer object
// retrieve the value and stringify it
// create new String object with the result value
// return its reference
static void native_integer_to_string(void)
{
  char buffer[100];
  Frame frame = getTopFrame();
  Reference this = peekOperandStack(frame, 0);
  sprintf(buffer, "%d", getIntegerValue(this));
  Reference ret = allocateString(buffer);
  (void) popOperandStack(frame);
  pushOperandStack(frame, ret);
  return;
}

// create one string from another
static void native_string_constructor_string(void)
{
  Frame frame = getTopFrame();
  Reference ref = popOperandStack(frame);
  Reference this = popOperandStack(frame);
  // UGLY! This leverages the current implementation of placing
  // the integer value for Integer objects and the pointer to the
  // string value for String objects in the same location in the
  // object: the second word of the header.
  // ALSO: this means multiple String objects can be pointing to
  // the same string value, which should be okay since the string
  // values are immutable.
  putIntegerValue(this, getIntegerValue(ref));
  return;
}

// pop reference to String object
// retrieve the value and compute its length
// create new Integer object with the length as its value
// return its reference
static void native_string_length(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference this = popOperandStack(frame);
  putIntegerValue(ret, strlen(getStringValue(this)));
  pushOperandStack(frame, ret);
  return;
}

// pop reference to Integer object for end index and retrieve its value
// pop reference to Integer object for start index and retrieve its value
// pop reference to String object and retrieve its value
// check for errors 
// copy string value into a buffer
// generate pointer to substring in the buffer
// create new String object for the substring
// return its reference
static void native_string_substr(void)
{
  Frame frame = getTopFrame();
  Reference endRef = peekOperandStack(frame, 0);
  Reference startRef = peekOperandStack(frame, 1);
  Reference this = peekOperandStack(frame, 2);
  int end = getIntegerValue(endRef);
  int start = getIntegerValue(startRef);
  const char *value = getStringValue(this);
  int length = strlen(value);
  if (length == 0)
  {
    haltVM( "substr invoked on String of length 0\n", -1);
  }
  if ((start >= length) || (start < 0))
  {
    haltVM("substr with illegal start index\n", -1);
  }
  if ((end >= length) || (end < 0))
  {
    haltVM("substr with illegal end index\n", -1);
  }
  if (end < start)
  {
    haltVM("substr invoked with end index less than start index\n", -1);
  }
  char buffer[length + 1];
  strcpy(buffer, value);
  buffer[end + 1] = 0;
  Reference ret = allocateString(buffer + start);
  (void) popOperandStack(frame);
  (void) popOperandStack(frame);
  (void) popOperandStack(frame);
  pushOperandStack(frame, ret);
  return;
}

// pop reference to String object
// get its value and convert to an integer
// check for errors
// create new Integer object with converted value
// push its reference
static void native_string_to_integer(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference this = popOperandStack(frame);
  const char *value = getStringValue(this);
  int length = strlen(value);
  if (length == 0)
  {
    haltVM("toInteger invoked on String of length 0\n", -1);
  }
  const char *ptr = (value[0] == '-') ? value + 1 : value;
  if (strspn(ptr, "0123456789") != strlen(value))
  {
    haltVM("invalid characters, cannot invoke toInteger\n", -1);
  }
  errno = 0;
  int retVal = strtol(value, NULL, 10);
  if (errno != 0)
  {
    haltVM("value out of range in toInteger\n", -1);
  }
  putIntegerValue(ret, retVal);
  pushOperandStack(frame, ret);
  return;
}

// pop two String references and retrieve their values
// concatenate the strings and create new String object for result
// push its reference
static void native_string_concat(void)
{
  Frame frame = getTopFrame();
  Reference ref = peekOperandStack(frame, 0);
  Reference this = peekOperandStack(frame, 1);
  const char *refValue = getStringValue(ref);
  const char *thisValue = getStringValue(this);
  char buffer[strlen(refValue) + strlen(thisValue) + 1];
  strcpy(buffer, thisValue);
  strcat(buffer, refValue);
  Reference ret = allocateString(buffer);
  (void) popOperandStack(frame);
  (void) popOperandStack(frame);
  pushOperandStack(frame, ret);
  return;
}

// pop two references to Strings and retrieve their values
// compare the values
// if the this value is greater than the non-this value
// then create an Integer object with value 1
// else create an Integer object with value 0
// push its reference
static void native_string_greater_than(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference ref = popOperandStack(frame);
  Reference this = popOperandStack(frame);
  const char *refValue = getStringValue(ref);
  const char *thisValue = getStringValue(this);
  if (strcmp(thisValue, refValue) > 0)
  {
    putIntegerValue(ret, 1);
  }
  else
  {
    putIntegerValue(ret, 0);
  }
  pushOperandStack(frame, ret);
  return;
}

// pop two references to Strings and retrieve their values
// compare the values
// if the this value is less than the non-this value
// then create an Integer object with value 1
// else create an Integer object with value 0
// push its reference
static void native_string_less_than(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference ref = popOperandStack(frame);
  Reference this = popOperandStack(frame);
  const char *refValue = getStringValue(ref);
  const char *thisValue = getStringValue(this);
  if (strcmp(thisValue, refValue) < 0)
  {
    putIntegerValue(ret, 1);
  }
  else
  {
    putIntegerValue(ret, 0);
  }
  pushOperandStack(frame, ret);
  return;
}

// pop reference to String object
// get its value and hash it using the Bernstein hash
// create new Integer object with the computed hash value
// push its reference
static void native_string_hash_code(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference this = popOperandStack(frame);
  const char *str = getStringValue(this);
  unsigned hashVal = 5381;
  int c;
  while ((c = *str++))
     hashVal = ((hashVal << 5) + hashVal) + c; /* hashVal * 33 + c */
  putIntegerValue(ret, hashVal);
  pushOperandStack(frame, ret);
  return;
}

// pop two references to Strings and retrieve their values
// compare the values
// if the this value is equal to the non-this value
// then create an Integer object with value 1
// else create an Integer object with value 0
// push its reference
static void native_string_equals(void)
{
  Frame frame = getTopFrame();
  Reference ret = allocateObject(getIntegerClass());
  Reference ref = popOperandStack(frame);
  Reference this = popOperandStack(frame);
  const char *refValue = getStringValue(ref);
  const char *thisValue = getStringValue(this);
  if (strcmp(thisValue, refValue) == 0)
  {
    putIntegerValue(ret, 1);
  }
  else
  {
    putIntegerValue(ret, 0);
  }
  pushOperandStack(frame, ret);
  return;
}

// do nothing: string is already a string
static void native_string_to_string(void)
{
  return;
}

static void native_table_constructor(void)
{
  haltVM("tables not implemented\n", -1);
}

static void native_table_constructor_integer(void)
{
  haltVM("tables not implemented\n", -1);
}

static void native_table_get(void)
{
  haltVM("tables not implemented\n", -1);
}

static void native_table_put(void)
{
  haltVM("tables not implemented\n", -1);
}

static void native_table_remove(void)
{
  haltVM("tables not implemented\n", -1);
}

static void native_table_first_key(void)
{
  haltVM("tables not implemented\n", -1);
}

static void native_table_next_key(void)
{
  haltVM("tables not implemented\n", -1);
}

// vector of function pointers to native methods
#define NATIVE_METHOD_VECTOR_LENGTH 43
static void (*(native_method_vector[]))(void) =
{
  /*  0 */ native_object_constructor,
  /*  1 */ native_object_equals,
  /*  2 */ native_object_hash_code,
  /*  3 */ native_object_to_string,
  /*  4 */ native_integer_constructor,
  /*  5 */ native_integer_constructor_integer,
  /*  6 */ native_integer_add,
  /*  7 */ native_integer_subtract,
  /*  8 */ native_integer_multiply,
  /*  9 */ native_integer_divide,
  /* 10 */ native_integer_greater_than,
  /* 11 */ native_integer_less_than,
  /* 12 */ native_integer_not,
  /* 13 */ native_integer_minus,
  /* 14 */ native_integer_add,
  /* 15 */ native_integer_subtract,
  /* 16 */ native_integer_multiply,
  /* 17 */ native_integer_divide,
  /* 18 */ native_integer_greater_than,
  /* 19 */ native_integer_less_than,
  /* 20 */ native_integer_not,
  /* 21 */ native_integer_minus,
  /* 22 */ native_integer_equals,
  /* 23 */ native_integer_hash_code,
  /* 24 */ native_integer_to_string,
  /* 25 */ native_string_constructor_string,
  /* 26 */ native_string_length,
  /* 27 */ native_string_substr,
  /* 28 */ native_string_to_integer,
  /* 29 */ native_string_concat,
  /* 30 */ native_string_concat,
  /* 31 */ native_string_greater_than,
  /* 32 */ native_string_less_than,
  /* 33 */ native_string_hash_code,
  /* 34 */ native_string_equals,
  /* 35 */ native_string_to_string,
  /* 36 */ native_table_constructor,
  /* 37 */ native_table_constructor_integer,
  /* 38 */ native_table_get,
  /* 39 */ native_table_put,
  /* 40 */ native_table_remove,
  /* 41 */ native_table_first_key,
  /* 42 */ native_table_next_key,
  0
};

// invoke a native method given its index
void invokeNativeMethod(Index index)
{
  if (index >= NATIVE_METHOD_VECTOR_LENGTH)
  {
    haltVM("invalid native method index", -1);
  }

  (*(native_method_vector[index]))(); 
}
