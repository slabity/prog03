//
// class.c
//
// Stubs for the functions to support classes in the maTe virtual
// machine. There are also two functions for supporting the main
// block.
//

#include <stdbool.h>

#include "vm.h"

extern long memory_size;
extern uint32_t *virtual_memory;

// validate a class descriptor
bool isValidClass(Class class)
{
    // For each class in the table
    for (int i = 0; i < virtual_memory[2]; ++i) {
        // Check if our class is valid
        if (getIthClass(i) == class)
            return true;
    }

    return false;
}

// returns the class descriptor for the predefined class Object
Class getObjectClass(void)
{
    return getIthClass(0);
}

// returns the class descriptor for the predefined class Integer
Class getIntegerClass(void)
{
    return getIthClass(1);
}

// returns the class descriptor for the predefined class String
Class getStringClass(void)
{
    return getIthClass(2);
}

// returns the class descriptor for the predefined class Table
Class getTableClass(void)
{
    return getIthClass(3);
}

// returns the class descriptor for the i-th class in the class Table
Class getIthClass(int i)
{
    // Check to make sure `i` is within bounds.
    int count = virtual_memory[2];
    if (i > count) {
        fprintf(stderr, "Cannot get class %d, only %d classes\n", i, count + 1);
    }

    uint32_t *class_table = &virtual_memory[3];
    uint32_t *pointer = class_table;

    for (int j = 0; j < i; ++j) {
        // Increment the pointer past the address.
        pointer++;

        // Increment the pointer past the name string
        while (*pointer != 0) {
            pointer++;
        }
        // Go past the null character
        pointer++;
    }

    return *pointer;
}

// returns true if the given class is the predefined class Object
bool isObjectClass(Class class)
{
    return getObjectClass() == class;
}

// returns true if the given class is the predefined class Integer
bool isIntegerClass(Class class)
{
    return getIntegerClass() == class;
}

// returns true if the given class is the predefined class String
bool isStringClass(Class class)
{
    return getStringClass() == class;
}

// returns true if the given class is the predefined class Table
bool isTableClass(Class class)
{
    return getTableClass() == class;
}

// returns the super class for the given class, or zero if there
// is no super class, ie the given class is Object
Class getSuper(Class class)
{
    uint32_t *pointer = &virtual_memory[class / sizeof(uint32_t)];
    return *pointer;
}

// returns the number of fields for the given class
Count getNumFields(Class class)
{
    uint32_t *pointer = &virtual_memory[class / sizeof(uint32_t)];
    pointer += 1;
    return *pointer;
}

// returns the number of methods for the given class
Count getNumMethods(Class class)
{
    uint32_t *pointer = &virtual_memory[class / sizeof(uint32_t)];
    pointer += 2;
    return *pointer;
}

// returns the address of a method given its class and its index
Address getMethodAddress(Class class, Index index)
{
    uint32_t *pointer = &virtual_memory[class / sizeof(uint32_t)];
    pointer += 2;

    // Check to make sure `i` is within bounds.
    uint32_t count = *pointer;
    if (index > count) {
        fprintf(stderr, "Cannot get %d method. Only %d exist\n", index, count);
    }

    pointer += 1;

    for (int j = 0; j < index; ++j) {
        // Increment the pointer past the address.
        pointer++;

        // Increment the pointer past the number of fields
        pointer++;

        // Increment the pointer past the name string
        while (*pointer != 0) {
            pointer++;
        }
        // Go past the null character
        pointer++;
    }

    return *pointer;
}

// returns the number of locals for a method given its class and its index
// note: if the method is a native method then this routine returns the
//       native method index.
Count getMethodNumLocals(Class class, Index index)
{
    uint32_t *pointer = &virtual_memory[class / sizeof(uint32_t)];
    pointer += 2;

    // Check to make sure `i` is within bounds.
    uint32_t count = *pointer;
    if (index > count) {
        fprintf(stderr, "Cannot get %d method. Only %d exist\n", index, count);
    }

    pointer += 1;

    for (int j = 0; j < index; ++j) {
        // Increment the pointer past the address.
        pointer++;

        // Increment the pointer past the number of fields
        pointer++;

        // Increment the pointer past the name string
        while (*pointer != 0) {
            pointer++;
        }
        // Go past the null character
        pointer++;
    }

    // We're on the address. Go to the next field.
    pointer++;

    return *pointer;
}

// reads word from address inside a method or constructor
// body, or from address inside a main block body. This
// can be used for all pieces of instructions. word is returned
// as an int.
int getWordFromCode(Address addr)
{
    if (addr > memory_size) {
        fprintf(stderr, "Attempted to get address %d, but memory size is %ld\n", addr, memory_size);
        exit(EXIT_FAILURE);
    }
    return virtual_memory[addr / 4];
}

/////////////////////////////////////////////////////////////////////
// support for the main block

// returns the address of the main block
Address getMainBlockAddress(void)
{
     return virtual_memory[0];
}

// returns the number of locals for the main block
Count getMainBlockNumLocals(void)
{
    return virtual_memory[1];
}

