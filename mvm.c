//
// main.c
//
// main function for the maTe VM
//
// IMPORTANT: This file should not contain any helper functions required
// by other files. When doing unit testing, I will replace this file
// with another file with a different main function, such as classTest.c
//

#include <stdio.h>
#include <stdlib.h>

#include "vm.h"

#define P_DEBUG 0

#if P_DEBUG
#define DEBUG_PRINT(...) fprintf(stderr, __VA_ARGS__);
#else
#define DEBUG_PRINT(...) ;
#endif

void newInt() {
    // Allocate an Integer
    Class type = getIntegerClass();
    Reference integer = allocateObject(type);

    // Increment the PC to the value
    Frame frame = getTopFrame();
    int pc = getPC(frame);
    pc += sizeof(Address);
    putPC(frame, pc);

    // Get the value.
    int value = getWordFromCode(pc);

    // Push the value to the operand stack.
    putIntegerValue(integer, value);
    pushOperandStack(frame, integer);

    DEBUG_PRINT("newInt %d\n", value);
}

void newStr() {
    // Create a buffer
    char buffer[256];

    Frame frame = getTopFrame();
    int pc = getPC(frame);

    // Get the value.
    int i = 0;
    while (true) {
        pc += sizeof(Address);
        char value = (char)getWordFromCode(pc);

        buffer[i] = value;
        ++i;

        if (value == '\0') {
            break;
        }
    }

    Reference str = allocateString(buffer);

    frame = getTopFrame();
    putPC(frame, pc);

    // Push the value to the operand stack.
    pushOperandStack(frame, str);

    DEBUG_PRINT("newStr %d\n", str);
}

void out() {
    // Pop the operand
    Frame frame = getTopFrame();
    Reference string = popOperandStack(frame);

    DEBUG_PRINT("out %d\n", string);

    printf("%s", getStringValue(string));
}

void in() {
    // Read a single word from stdin
    char buffer[256];
    scanf(" %49[^ \t.\n]%*c", buffer);

    Reference str = allocateString(buffer);
    Frame frame = getTopFrame();
    pushOperandStack(frame, str);
}

void areturn() {
    // Pop the last operand and push it onto the previous frame
    Frame current = popFrame();

    // But check if the frame stack is empty.
    if (isEmptyFrameStack()) {
        haltVM("Process finished successfully", 0);
    }

    Frame previous = getTopFrame();
    Reference obj = popOperandStack(current);
    pushOperandStack(previous, obj);

    DEBUG_PRINT("areturn\n");
}

void aconst_null() {
    // Throw a null reference onto the stack
    Frame frame = getTopFrame();
    Reference null = 0;
    pushOperandStack(frame, null);

    DEBUG_PRINT("aconst_null\n");
}

void aload() {
    // Get the index argument
    Frame frame = getTopFrame();
    int pc = getPC(frame);
    pc += sizeof(Address);
    putPC(frame, pc);
    Index index = getWordFromCode(pc);

    // Load the local variable and push it to the stack.
    Reference local = getLocals(frame, index);
    pushOperandStack(frame, local);

    DEBUG_PRINT("aload %d\n", local);
}

void astore() {
    // Get the index argument
    Frame frame = getTopFrame();
    int pc = getPC(frame);
    pc += sizeof(Address);
    putPC(frame, pc);
    Index index = getWordFromCode(pc);

    // Load the local variable and push it to the stack.
    Reference op = popOperandStack(frame);
    putLocals(frame, index, op);

    DEBUG_PRINT("astore\n");
}

void dup() {
    // Get the top operand
    Frame frame = getTopFrame();
    Reference op = popOperandStack(frame);

    // Push it twice.
    pushOperandStack(frame, op);
    pushOperandStack(frame, op);

    DEBUG_PRINT("dup\n");
}

void dup_x1() {
    // Get the top two operands
    Frame frame = getTopFrame();
    Reference op1 = popOperandStack(frame);
    Reference op2 = popOperandStack(frame);

    // Push them in order
    pushOperandStack(frame, op1);
    pushOperandStack(frame, op2);
    pushOperandStack(frame, op1);

    DEBUG_PRINT("dup_x1\n");
}

void ifeq() {
    // Get the integer
    Frame frame = getTopFrame();
    Reference op = popOperandStack(frame);
    int check = getIntegerValue(op);

    // Get the address
    int pc = getPC(frame);
    pc += sizeof(Address);
    putPC(frame, pc);
    Address addr = getWordFromCode(pc);

    DEBUG_PRINT("ifeq %d\n", check);

    if (check == 0) {
        putPC(frame, addr);
    }
}

void _goto() {
    // Get the address to go to
    Frame frame = getTopFrame();
    int pc = getPC(frame);
    pc += sizeof(Address);
    putPC(frame, pc);
    Address addr = getWordFromCode(pc);

    putPC(frame, addr - sizeof(Address));

    DEBUG_PRINT("goto\n");
}

void new() {
    // Get the class to allocate
    Frame frame = getTopFrame();
    int pc = getPC(frame);
    pc += sizeof(Address);
    putPC(frame, pc);
    Class cl = getWordFromCode(pc);

    // Put the object on the stack.
    Reference obj = allocateObject(cl);
    pushOperandStack(frame, obj);

    DEBUG_PRINT("new %d\n", cl);
}

void _getField() {
    // Get the index of the field we want.
    Frame frame = getTopFrame();
    int pc = getPC(frame);
    pc += sizeof(Address);
    putPC(frame, pc);
    Index i = getWordFromCode(pc);

    // Get the object to get the field
    Reference obj = popOperandStack(frame);
    Reference field = getField(obj, i);

    // Push the field to the stack
    pushOperandStack(frame, field);

    DEBUG_PRINT("getField %d\n", field);
}

void _putField() {
    // Get the index of the field we want.
    Frame frame = getTopFrame();
    int pc = getPC(frame);
    pc += sizeof(Address);
    putPC(frame, pc);
    Index i = getWordFromCode(pc);

    // Get the field and object
    Reference field = popOperandStack(frame);
    Reference obj = popOperandStack(frame);

    // Put the field in the object.
    putField(obj, i, field);

    DEBUG_PRINT("putField %d\n", field);
}

void invokeVirtual() {
    // Get the index of the method.
    Frame frame = getTopFrame();
    int pc = getPC(frame);
    pc += sizeof(Address);
    Index i = getWordFromCode(pc);

    // Get the number of methods.
    pc += sizeof(Address);
    int n = getWordFromCode(pc);

    // Update the PC
    putPC(frame, pc);

    // Allocate a new frame. Don't set the PC yet.
    Frame new = createFrame(n, 0);

    // Put all the locals in the new frame.
    DEBUG_PRINT("\tIndex %d, n %d\n", i, n);
    for (int j = 1; j < n; ++j) {
        Reference local = popOperandStack(frame);
        DEBUG_PRINT("\tPopping %d\n", local);
        putLocals(new, n - j, local);
    }

    // Get the object to invoke on
    //pc += sizeof(Address);
    Reference obj = popOperandStack(frame);
    putLocals(new, 0, obj);

    // Get the method to invoke.
    Class cl = getClass(obj);
    Address addr = getMethodAddress(cl, i);

    // Check if it's a special native code.
    if (addr == 0) {
        DEBUG_PRINT("\tInvoking native from virtual\n");

        // Put the object back onto the stack.
        obj = getLocals(new, 0);
        pushOperandStack(frame, obj);

        DEBUG_PRINT("\tPutting back object %d\n", obj);

        // Put the local variables back on the stack.
        for (int j = 1; j < n; ++j) {
            Reference local = getLocals(new, j);
            pushOperandStack(frame, local);
        }

        // Delete the new frame.
        deleteFrame(new);

        // Get the native opcode
        Index native = getMethodNumLocals(cl, i);
        invokeNativeMethod(native);
    } else {
        DEBUG_PRINT("invokeVirtual\n");
        // Put the PC right before the method.
        putPC(new, addr - sizeof(Address));
        pushFrame(new);
    }
}

void invokeSpecial() {
    // Get the index of the method.
    Frame frame = getTopFrame();
    int pc = getPC(frame);

    pc += sizeof(Address);
    Address addr = getWordFromCode(pc);

    // Get the number of args
    pc += sizeof(Address);
    int n = getWordFromCode(pc);

    // Get the number of locals
    pc += sizeof(Address);
    int locals = getWordFromCode(pc);

    // Update the PC
    putPC(frame, pc);

    // Allocate a new frame with the correct PC
    // Remember, one address before the actuall addr since the fetch will
    // increment it anyways.
    Frame new = createFrame(locals, addr - sizeof(Address));

    // Put all the locals in the new frame.
    for (int j = 1; j < n; ++j) {
        Reference local = popOperandStack(frame);
        DEBUG_PRINT("\tPopping %d\n", local);
        putLocals(new, n - j, local);
    }

    // Put the object into the locals too
    Reference obj = popOperandStack(frame);
    putLocals(new, 0, obj);

    pushFrame(new);
}

void invokeNative() {
    // Get the native index
    Frame frame = getTopFrame();
    int pc = getPC(frame);
    pc += sizeof(Address);
    Index i = getWordFromCode(pc);
    pushOperandStack(frame, i);
    putPC(frame, pc);

    DEBUG_PRINT("invokeNative %d\n", i);

    // Call the invokeNativeMethod function
    invokeNativeMethod(i);
}

void checkCast() {
    // Get the index of the method.
    Frame frame = getTopFrame();
    int pc = getPC(frame);

    // Get the class we want to compare it to.
    pc += sizeof(Address);
    Class cl = getWordFromCode(pc);
    putPC(frame, pc);

    // Get the two reference ops
    Reference ref2 = popOperandStack(frame);
    Reference ref1 = popOperandStack(frame);

    // Create an integer
    Class type = getIntegerClass();
    Reference integer = allocateObject(type);

    // If ref2 has a value of zero
    if (getIntegerValue(ref2) == 0) {
        // Check if ref1 is null or a type of cl
        if (ref1 == 0 || ref1 == cl) {
            putIntegerValue(integer, 1);
        } else {
            putIntegerValue(integer, 0);
        }
        pushOperandStack(frame, integer);
    } else {
        if (ref1 == 0 || ref1 == cl) {
            pushOperandStack(frame, ref1);
        } else {
            haltVM("checkCast called halt with improper operands", -1);
        }
    }
}

void _return() {
    Frame frame = popFrame();
    deleteFrame(frame);
}

void refcmp() {
    // Get the index of the method.
    Frame frame = getTopFrame();

    Reference ref2 = popOperandStack(frame);
    Reference ref1 = popOperandStack(frame);

    // Create an integer
    Class type = getIntegerClass();
    Reference integer = allocateObject(type);

    if (ref2 == ref1) {
        putIntegerValue(integer, 1);
    } else {
        putIntegerValue(integer, 0);
    }

    pushOperandStack(frame, integer);
}

void pop() {
    Frame frame = getTopFrame();
    popOperandStack(frame);
}

// Executes the instruction and put the PC in the frame.
void executeInstruction(int instruction) {
    // Execute the instruction
    switch (instruction) {
        case 1:
            aconst_null();
            break;
        case 25:
            aload();
            break;
        case 58:
            astore();
            break;
        case 89:
            dup();
            break;
        case 153:
            ifeq();
            break;
        case 167:
            _goto();
            break;
        case 176:
            areturn();
            break;
        case 180:
            _getField();
            break;
        case 181:
            _putField();
            break;
        case 182:
            invokeVirtual();
            break;
        case 183:
            invokeSpecial();
            break;
        case 187:
            new();
            break;
        case 243:
            in();
            break;
        case 244:
            invokeNative();
        case 245:
            out();
            break;
        case 246:
            _return();
            break;
        case 247:
            newInt();
            break;
        case 248:
            newStr();
            break;
        case 249:
            refcmp();
            break;
        case 250:
            pop();
            break;
        case 251:
            dup_x1();
            break;
        default:
            DEBUG_PRINT("Invalid instruction %d\n", instruction);
            haltVM("Invalid instruction", -1);
    }
}

// Recursively execute each method
void execute(Address pc, Count locals) {
    DEBUG_PRINT("Executing %d with %d locals...\n", pc, locals);

    // Create and push the frame.
    Frame start = createFrame(locals, pc);
    pushFrame(start);

    while (true) {
        // Fetch the next instruction.
        Frame frame = getTopFrame();
        pc = getPC(frame);
        int instruction = getWordFromCode(pc);

        DEBUG_PRINT("Instruction %d at %d...\n", instruction, pc);

        // Execute the instruction
        executeInstruction(instruction);

        // We should be right before the next instruction. Let's increment it.
        frame = getTopFrame();
        pc = getPC(frame);
        pc += sizeof(Address);
        putPC(frame, pc);
    }

    // Pop and destroy the frame.
    Frame end = popFrame();
    deleteFrame(end);
}

// Execute the main block method.
void execute_main() {
    Address pc = getMainBlockAddress();
    Count locals = getMainBlockNumLocals();
    execute(pc, locals);
}

int main(int argc, char *argv[])
{
    // expect the class file name to be on the command line
    if (argc != 2)
    {
        fprintf(stderr, "Usage: mvm classFile\n");
        exit(-1);
    }

    // Initialize the VM with the given classfile
    initializeVM(0, argv[1]);

    execute_main();

    return 0;
}

