//
// unit test for maTe VM Implementation
//
// Test the functions that implement the heap:
//
// This test assumes that the functions that provide access to the class
// file are working. And, of course, this function uses initializeVM
// to read the class file and do any initialization that is required to
// support the above functions.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vm.h"

int main(int argc, char *argv[])
{
  // expects no arguments because we are using a particular class file
  if (argc != 1)
  {
    fprintf(stderr, "Usage: heapTest\n");
    exit(-1);
  }
  
  // initialize: need to use a class with a user class that has at least
  // four fields
  initializeVM(10000, "userClass.class");

  // okay, begin the testing...

  Class A = getIthClass(4);

  Reference objectRef = allocateObject(getObjectClass());
  Reference integerRef = allocateObject(getIntegerClass());
  Reference aRef = allocateObject(A);
  Reference stringRef = allocateString("abcdefghijklmnopqrstuvwxyz");

  if (getObjectClass() != getClass(objectRef))
  {
    fprintf(stderr, "FAIL: getClass from objectRef\n");
  }

  if (getIntegerClass() != getClass(integerRef))
  {
    fprintf(stderr, "FAIL: getClass from integerRef\n");
  }

  if (getStringClass() != getClass(stringRef))
  {
    fprintf(stderr, "FAIL: getClass from stringRef\n");
  }

  if (A != getClass(aRef))
  {
    fprintf(stderr, "FAIL: getClass from aRef\n");
  }

  if (strcmp(getStringValue(stringRef), "abcdefghijklmnopqrstuvwxyz"))
  {
    fprintf(stderr, "FAIL: getStringValue\n");
  }

  putIntegerValue(integerRef, 1066);
  if (getIntegerValue(integerRef) != 1066)
  {
    fprintf(stderr, "FAIL: getIntegerValue\n");
  }

  putField(aRef, 0, integerRef);
  if (getField(aRef, 0) != integerRef)
  {
    fprintf(stderr, "FAIL: getField 0\n");
  }

  putField(aRef, 3, objectRef);
  if (getField(aRef, 3) != objectRef)
  {
    fprintf(stderr, "FAIL: getField 3\n");
  }

  printf("testing complete.\n");

  return 0;
}
