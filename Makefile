#
# Makefile for the maTe VM: mmv
#   and three executables for unit testing:
#     classTest, heapTest and frameTest
#

CC = gcc

CFLAGS = -g -Wall -std=c99

mvm: mvm.o vm.o heap.o frame.o class.o native.o
	$(CC) $(CFLAGS) mvm.o vm.o heap.o frame.o class.o native.o -o mvm

classTest: classTest.o vm.o heap.o frame.o class.o native.o
	$(CC) $(CFLAGS) classTest.o vm.o heap.o frame.o class.o native.o -o classTest

frameTest: frameTest.o vm.o heap.o frame.o class.o native.o
	$(CC) $(CFLAGS) frameTest.o vm.o heap.o frame.o class.o native.o -o frameTest

heapTest: heapTest.o vm.o heap.o frame.o class.o native.o
	$(CC) $(CFLAGS) heapTest.o vm.o heap.o frame.o class.o native.o -o heapTest

classTest.o: classTest.c vm.h
	$(CC) $(CFLAGS) -c classTest.c

frameTest.o: frameTest.c vm.h
	$(CC) $(CFLAGS) -c frameTest.c

heapTest.o: heapTest.c vm.h
	$(CC) $(CFLAGS) -c heapTest.c

mvm.o: mvm.c vm.h
	$(CC) $(CFLAGS) -c mvm.c

native.o: native.c vm.h
	$(CC) $(CFLAGS) -c native.c

vm.o: vm.c vm.h
	$(CC) $(CFLAGS) -c vm.c

heap.o: heap.c vm.h
	$(CC) $(CFLAGS) -c heap.c

frame.o: frame.c vm.h
	$(CC) $(CFLAGS) -c frame.c

class.o: class.c vm.h
	$(CC) $(CFLAGS) -c class.c

clean:
	-rm -f *.o mvm heapTest classTest frameTest

