// unit test for maTe VM implementation
//
// Test the functions that implement the frame stack:
//
// This test is pretty much stand-alone. It passes a class file to
// initializeVM but it does not access the class file. It also does
// not use the heap.

#include <stdio.h>
#include <stdlib.h>

#include "vm.h"

int main(int argc, char *argv[])
{
  // expects no arguments because we are using a particular class file
  if (argc != 1)
  {
    fprintf(stderr, "Usage: frameTest\n");
    exit(-1);
  }

  // initialize
  initializeVM(10000, "first.class");

  // okay, begin the testing...

  Address pc = 100;
  Reference ref1 = (Reference) 200;
  Reference ref2 = (Reference) 300;

  Frame f = createFrame(10, pc);

  if (!isEmptyOperandStack(f))
  {
    fprintf(stderr, "FAIL: isEmptyOperandStack when empty\n");
  }

  pushOperandStack(f, ref1);
  pushOperandStack(f, ref2);

  if (isEmptyOperandStack(f))
  {
    fprintf(stderr, "FAIL: isEmptyOperandStack when not empty\n");
  }

  if (peekOperandStack(f, 0) != ref2)
  {
    fprintf(stderr, "FAIL: peekOperandStack 0\n");
  }

  if (peekOperandStack(f, 1) != ref1)
  {
    fprintf(stderr, "FAIL: peekOperandStack 1\n");
  }

  if (popOperandStack(f) != ref2)
  {
    fprintf(stderr, "FAIL: popOperandStack #1\n");
  }

  if (popOperandStack(f) != ref1)
  {
    fprintf(stderr, "FAIL: popOperandStack #2\n");
  }

  if (lengthLocals(f) != 10)
  {
    fprintf(stderr, "FAIL: lengthLocals\n");
  }

  int i;
  for (i = 0; i < 10; i++)
  {
    putLocals(f, i, (Reference) i);
  }
  for (i = 0; i < 10; i++)
  {
    if (getLocals(f, i) != ((Reference) i))
    {
      fprintf(stderr, "FAIL: getLocals %d\n", i);
    }
  }

  if (getPC(f) != pc)
  {
    fprintf(stderr, "FAIL: getPC #1\n");
  }

  putPC(f, pc + 100);
  if (getPC(f) != (pc + 100))
  {
    fprintf(stderr, "FAIL: getPC #2\n");
  }

  if (!isEmptyFrameStack())
  {
    fprintf(stderr, "FAIL: isEmptyFrameStack when empty\n");
  }

  pushFrame(f);

  if (isEmptyFrameStack())
  {
    fprintf(stderr, "FAIL: isEmptyFrameStack when not empty\n");
  }

  Frame f2 = createFrame(5, pc + 1000);
  pushFrame(f2);

  if (getTopFrame() != f2)
  {
    fprintf(stderr, "FAIL: getTopFrame\n");
  }

  if (popFrame() != f2)
  {
    fprintf(stderr, "FAIL: popFrame #1\n");
  }

  if (popFrame() != f)
  {
    fprintf(stderr, "FAIL: popFrame #2\n");
  }

  deleteFrame(f2);
  deleteFrame(f);

  printf("testing complete.\n");

  haltVM("DONE", 0);

  return 0;
}
