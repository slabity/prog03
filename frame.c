//
// frame.c
//
// Stubs for functions to support frames in the maTe virtual
// machine. A frame contains a local slot array, a PC, and an
// operand stack.
//

#include <stdbool.h>

#include "vm.h"

typedef struct {
    Address pc;
    Count localsLength;
    Index op_index;
    Reference *locals;
} FrameStruct;

// create a frame given its local slot array length and its initial PC
Frame createFrame(Count localsLength, Address pc)
{
    FrameStruct *frame = malloc(sizeof(FrameStruct));
    frame->pc = pc;
    frame->localsLength = localsLength;
    frame->op_index = 0;
    frame->locals = malloc(sizeof(Reference) * localsLength);

    return (Frame)frame;
}

// delete a frame
void deleteFrame(Frame frame)
{
    FrameStruct *fstruct = (FrameStruct *)frame;
    free(fstruct->locals);
    free(fstruct);
}

// is operand stack in frame empty?
bool isEmptyOperandStack(Frame frame)
{
    FrameStruct *fstruct = (FrameStruct *)frame;
    return fstruct->op_index == 0;
}

// push reference onto operand stack of frame
void pushOperandStack(Frame frame, Reference ref)
{
    FrameStruct *fstruct = (FrameStruct *)frame;
    Reference *locals = fstruct->locals;
    locals[fstruct->op_index] = ref;
    fstruct->op_index += 1;
}

// pop reference from operand stack of frame
Reference popOperandStack(Frame frame)
{
    FrameStruct *fstruct = (FrameStruct *)frame;
    Reference *locals = (Reference *)fstruct->locals;
    Reference op = locals[fstruct->op_index - 1];
    fstruct->op_index -= 1;
    return op;
}

// retrieve the i-th item on the operand stack of frame (0 is the top,
// 1 is one beneath the top, 2 is two beneath the top, etc)
Reference peekOperandStack(Frame frame, Index i)
{
    FrameStruct *fstruct = (FrameStruct *)frame;
    Reference *locals = (Reference *)fstruct->locals;
    return locals[fstruct->op_index - (i + 1)];
}

// return length of local slot array of frame
Count lengthLocals(Frame frame)
{
    FrameStruct *fstruct = (FrameStruct *)frame;
    return fstruct->localsLength;
}

// get reference from local slot array of frame given its index
Reference getLocals(Frame frame, Index index)
{
    FrameStruct *fstruct = (FrameStruct *)frame;
    Reference *locals = (Reference *)fstruct->locals;
    return locals[index];
}

// put reference into local slot array of frame given its index
void putLocals(Frame frame, Index index, Reference ref)
{
    FrameStruct *fstruct = (FrameStruct *)frame;
    Reference *locals = (Reference *)fstruct->locals;
    locals[index] = ref;
}

// get PC from frame
Address getPC(Frame frame)
{
    FrameStruct *fstruct = (FrameStruct *)frame;
    return fstruct->pc;
}

// put PC into frame
void putPC(Frame frame, Address pc)
{
    FrameStruct *fstruct = (FrameStruct *)frame;
    fstruct->pc = pc;
}

/////////////////////////////////////////////////////////////////////
// support for the frame stack
//   note: the frame stack (and there is only one) is created by
//         initializeVM.

extern const long stack_size;
extern void **stack_memory;
uint32_t stack_index = 0;

// is frame stack empty?
bool isEmptyFrameStack(void)
{
    return stack_index == 0;
}

// push frame onto frame stack
void pushFrame(Frame frame)
{
    stack_memory[stack_index] = frame;
    stack_index += 1;
}

// pop frame from frame stack
Frame popFrame(void)
{
    stack_index -= 1;
    return stack_memory[stack_index];
}

// get top frame from frame stack but do not pop
Frame getTopFrame(void)
{
    return stack_memory[stack_index - 1];
}

