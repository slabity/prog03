//
// heap.c
//
// Stubs for functions to allocate and access memory for objects in
// the maTe virtual machine.
//

#include <stdbool.h>
#include <string.h>

#include "vm.h"

// function to allocate an object given its class
Reference allocateObject(Class class)
{
    // Size of the object is the number of fields and two words for a header
    size_t object_size = getNumFields(class) + 2;
    uint32_t *object = (uint32_t *)malloc(sizeof(uint32_t) * object_size);

    object[0] = class;

    return (Reference)object;
}

// function to allocate a String object given its initial value
// note: need a separate allocation function because of its variable length
// note: initial value must be copied into the new object, of course
// note: input string is the usual null-terminated character string
Reference allocateString(const char *initialValue)
{
    // Allocate the size for the string
    size_t str_size = strlen(initialValue);
    char *string = (char *)malloc(str_size);

    // Copy the string
    strcpy(string, initialValue);

    Reference object = allocateObject(getStringClass());

    uint32_t *words = (uint32_t *)object;

    words[1] = string;

    return object;
}

// function to retrieve class from object given its reference
Class getClass(Reference ref)
{
    uint32_t *object = (uint32_t *)ref;
    return object[0];
}

// get string value from String object
// note: the return value should not be changed
// note: the return value is the usual null-terminated character string
const char *getStringValue(Reference ref)
{
    uint32_t *object = (uint32_t *)ref;
    return (char *)object[1];
}

// get integer value from Integer object
int getIntegerValue(Reference ref)
{
    uint32_t *object = (uint32_t *)ref;
    return (int)object[1];
}

// put integer value into Integer Object
void putIntegerValue(Reference ref, int value)
{
    uint32_t *object = (uint32_t *)ref;
    object[1] = (uint32_t)value;
}

// function to retrieve field from object given its reference and
// the field index
Reference getField(Reference ref, Index index)
{
    int i = index + 2;
    uint32_t *object = (uint32_t *)ref;
    return object[i];
}

// function to update field in object given its reference,
// the field index and the reference to store in the field
void putField(Reference objectRef, Index index, Reference newFieldValue)
{
    int i = index + 2;
    uint32_t *object = (uint32_t *)objectRef;
    object[i] = (uint32_t)newFieldValue;
}

