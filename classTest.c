//
// unit test for maTe VM Implementation
//
// Test the functions that provide access to the class file:
//
// Also, will need to utilize a preliminary implementation of initializeVM
// to read the class file and do any initialization that is required to
// support the above functions.
//
// This requires a class file with at least one user class, which has at
// least five methods.

#include <stdio.h>
#include <stdlib.h>

#include "vm.h"

int main(int argc, char *argv[])
{
  // expect the class file name to be on the command line
  if (argc != 2)
  {
    fprintf(stderr, "Usage: classTest classFile\n");
    exit(-1);
  }
  
  // initialize: this routine should exit with an error message if
  // the class file name is no good
  initializeVM(10000, argv[1]);

  // okay, begin the testing...

  printf("\n");

  Address mainBlockAddress = getMainBlockAddress();
  printf("main block address is %x.\n", mainBlockAddress);
  printf("main block num locals is %x.\n", getMainBlockNumLocals());
  printf("first ten words of main block are:\n");
  int i;
  for (i = 0; i < 10; i++)
  {
    printf("  %08x\n", getWordFromCode(mainBlockAddress + (i * 4)));
  } 

  printf("\n");

  Class objectClass = getObjectClass();
  Class integerClass = getIntegerClass();
  Class stringClass = getStringClass();
  Class tableClass = getTableClass();
  Class userClass = getIthClass(4);
  printf("Object class is %08x.\n", objectClass);
  printf("Integer class is %08x.\n", integerClass);
  printf("String class is %08x.\n", stringClass);
  printf("Table class is %08x.\n", tableClass);
  printf("User class is %08x.\n", userClass);
  printf("should print 1 1 1 1: %d %d %d %d.\n",
    isObjectClass(objectClass),
    isIntegerClass(integerClass),
    isStringClass(stringClass),
    isTableClass(tableClass));
  printf("should print 0 0 0 0: %d %d %d %d.\n",
    isObjectClass(integerClass),
    isIntegerClass(tableClass),
    isStringClass(objectClass),
    isTableClass(stringClass));
  printf("should print 1 1 1 1 1: %d %d %d %d %d.\n",
    isValidClass(objectClass),
    isValidClass(integerClass),
    isValidClass(stringClass),
    isValidClass(tableClass),
    isValidClass(userClass));
  printf("should print 0 0: %d %d.\n",
    isValidClass(0),
    isValidClass(100000));

  printf("\n");

  printf("super class of Object is %08x.\n", getSuper(objectClass));
  printf("super class of Integer is %08x.\n", getSuper(integerClass));
  printf("super class of String is %08x.\n", getSuper(stringClass));
  printf("super class of Table is %08x.\n", getSuper(tableClass));
  printf("super class of user class is %08x.\n", getSuper(userClass));
  printf("number fields of Object is %d.\n", getNumFields(objectClass));
  printf("number fields of Integer is %d.\n", getNumFields(integerClass));
  printf("number fields of String is %d.\n", getNumFields(stringClass));
  printf("number fields of Table is %d.\n", getNumFields(tableClass));
  printf("number fields of user class is %d.\n", getNumFields(userClass));
  printf("number methods of Object is %d.\n", getNumMethods(objectClass));
  printf("number methods of Integer is %d.\n", getNumMethods(integerClass));
  printf("number methods of String is %d.\n", getNumMethods(stringClass));
  printf("number methods of Table is %d.\n", getNumMethods(tableClass));
  printf("number methods of user class is %d.\n", getNumMethods(userClass));

  printf("\n");

  printf("method 1 address and number of locals for Object: %08x %d.\n",
    getMethodAddress(objectClass, 1),
    getMethodNumLocals(objectClass, 1));
  printf("method 11 address and number of locals for Integer: %08x %d.\n",
    getMethodAddress(integerClass, 11),
    getMethodNumLocals(integerClass, 11));
  printf("method 7 address and number of locals for String: %08x %d.\n",
    getMethodAddress(stringClass, 7),
    getMethodNumLocals(stringClass, 7));
  printf("method 3 address and number of locals for Table: %08x %d.\n",
    getMethodAddress(tableClass, 3),
    getMethodNumLocals(tableClass, 3));
  printf("method 4 address and number of locals for user class: %08x %d.\n",
    getMethodAddress(userClass, 4),
    getMethodNumLocals(userClass, 4));

  printf("first ten words of method 4 of user class:\n");
  Address userMethodAddress = getMethodAddress(userClass, 4);
  for (i = 0; i < 10; i++)
  {
    printf("  %08x\n", getWordFromCode(userMethodAddress + (i * 4)));
  }
 
  printf("\n");

  printf("testing complete.\n");

  return 0;
}
