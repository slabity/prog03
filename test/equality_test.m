class A {
}

class B extends A {
}

Integer main() {
    A a, aa;
    B b;

    a = new A();

    aa = a;

    if (aa == a)
    {
       out 1; out newline;
    }
    else
    {
       out 0; out newline;
    }

    return 0;
}
