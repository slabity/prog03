class A {

  A()
  {
    i = 19;
  }

  Integer i;

  Integer get()
  {
    return i;
  }
}

class B extends A {

  B()
  {
    j = 99;
  }
 
  Integer j;

  Integer get()
  {
    return j + (super.get() * 10);
  }
}

Integer main()
{
  B b;

  b = new B();

  out (b.get()).toString(); out newline;
}
