# ClassDecNode
# ClassDecNode
# MainBlockNode
$mainBlock 1 
6
$Object "Object"
$Integer "Integer"
$String "String"
$Table "Table"
$A "A"
$B "B"
Object:
  0
  0
  3
  0 1 "Object$equals$Object"
  0 2 "Object$hashCode"
  0 3 "Object$toString"
Integer:
  $Object
  0
  19
  0 22 "Integer$equals$Object"
  0 23 "Integer$hashCode"
  0 24 "Integer$toString"
  0 6 "Integer$add$Integer"
  0 14 "Integer$operator+$Integer"
  0 7 "Integer$subtract$Integer"
  0 15 "Integer$operator-$Integer"
  0 8 "Integer$multiply$Integer"
  0 16 "Integer$operator*$Integer"
  0 9 "Integer$divide$Integer"
  0 17 "Integer$operator/$Integer"
  0 10 "Integer$greaterThan$Integer"
  0 18 "Integer$operator>$Integer"
  0 11 "Integer$lessThan$Integer"
  0 19 "Integer$operator<$Integer"
  0 12 "Integer$not"
  0 20 "Integer$operator!"
  0 13 "Integer$minus"
  0 21 "Integer$operator-"
String:
  $Object
  0
  10
  0 34 "String$equals$Object"
  0 33 "String$hashCode"
  0 35 "String$toString"
  0 26 "String$length"
  0 27 "String$substr$Integer$Integer"
  0 29 "String$concat$String"
  0 28 "String$toInteger"
  0 30 "String$operator+$String"
  0 31 "String$operator>$String"
  0 32 "String$operator<$String"
Table:
  $Object
  0
  8
  0 1 "Object$equals$Object"
  0 2 "Object$hashCode"
  0 3 "Object$toString"
  0 38 "Table$get$Object"
  0 39 "Table$put$Object$Object"
  0 40 "Table$remove$Object"
  0 41 "Table$firstKey"
  0 42 "Table$nextKey"
A:
  $Object
  1
  4
  0 1 "Object$equals$Object"
  0 2 "Object$hashCode"
  0 3 "Object$toString"
  $A$get 1 "A$get"
B:
  $A
  2
  4
  0 1 "Object$equals$Object"
  0 2 "Object$hashCode"
  0 3 "Object$toString"
  $B$get 1 "B$get"
mainBlock:
# BlockStatementNode
# LocalVarDecStatementNode
  aconst_null
  astore 0
# ExpressionStatementNode
# AssignmentOperatorNode
# VarNode
# ClassInstanceCreationExpressionNode
  new $B
  dup
  invokespecial $B_constructor 1 1
  astore 0
# OutStatementNode
# MethodInvocationExpressionNode
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 0
  invokevirtual 3 1
  invokevirtual 2 1
  out
# OutStatementNode
# StringLiteralNode
  newstr "
"
  out
  newint 0
mainBlock$.exit:
  areturn
# ClassConstructorNode
A_constructor:
# BlockStatementNode
# ConstructorInvocationStatementNode
# SuperExpressionNode
  aload 0
  invokenative 0
# ExpressionStatementNode
# AssignmentOperatorNode
# ClassFieldNode
  aload 0
# IntegerLiteralNode
  newint 19
  putfield 0
A_constructor$.exit:
  return
# ClassMethodNode
A$get:
# BlockStatementNode
# ReturnStatementNode
# DerefNode
# ClassFieldNode
  aload 0
  getfield 0
  goto $A$get$.exit
  aconst_null
A$get$.exit:
  areturn
# ClassConstructorNode
B_constructor:
# BlockStatementNode
# ConstructorInvocationStatementNode
# SuperExpressionNode
  aload 0
  invokespecial $A_constructor 1 1
# ExpressionStatementNode
# AssignmentOperatorNode
# ClassFieldNode
  aload 0
# IntegerLiteralNode
  newint 99
  putfield 1
B_constructor$.exit:
  return
# ClassMethodNode
B$get:
# BlockStatementNode
# ReturnStatementNode
# MethodInvocationExpressionNode
# DerefNode
# ClassFieldNode
  aload 0
  getfield 1
# MethodInvocationExpressionNode
# MethodInvocationExpressionNode
# SuperExpressionNode
  aload 0
  invokespecial $A$get 1 1
# IntegerLiteralNode
  newint 10
  invokevirtual 8 2
  invokevirtual 4 2
  goto $B$get$.exit
  aconst_null
B$get$.exit:
  areturn
