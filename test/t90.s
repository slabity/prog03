# ClassDecNode
# ClassDecNode
# MainBlockNode
$mainBlock 2 
6
$Object "Object"
$Integer "Integer"
$String "String"
$Table "Table"
$A "A"
$B "B"
Object:
  0
  0
  3
  0 1 "Object$equals$Object"
  0 2 "Object$hashCode"
  0 3 "Object$toString"
Integer:
  $Object
  0
  19
  0 22 "Integer$equals$Object"
  0 23 "Integer$hashCode"
  0 24 "Integer$toString"
  0 6 "Integer$add$Integer"
  0 14 "Integer$operator+$Integer"
  0 7 "Integer$subtract$Integer"
  0 15 "Integer$operator-$Integer"
  0 8 "Integer$multiply$Integer"
  0 16 "Integer$operator*$Integer"
  0 9 "Integer$divide$Integer"
  0 17 "Integer$operator/$Integer"
  0 10 "Integer$greaterThan$Integer"
  0 18 "Integer$operator>$Integer"
  0 11 "Integer$lessThan$Integer"
  0 19 "Integer$operator<$Integer"
  0 12 "Integer$not"
  0 20 "Integer$operator!"
  0 13 "Integer$minus"
  0 21 "Integer$operator-"
String:
  $Object
  0
  10
  0 34 "String$equals$Object"
  0 33 "String$hashCode"
  0 35 "String$toString"
  0 26 "String$length"
  0 27 "String$substr$Integer$Integer"
  0 29 "String$concat$String"
  0 28 "String$toInteger"
  0 30 "String$operator+$String"
  0 31 "String$operator>$String"
  0 32 "String$operator<$String"
Table:
  $Object
  0
  8
  0 1 "Object$equals$Object"
  0 2 "Object$hashCode"
  0 3 "Object$toString"
  0 38 "Table$get$Object"
  0 39 "Table$put$Object$Object"
  0 40 "Table$remove$Object"
  0 41 "Table$firstKey"
  0 42 "Table$nextKey"
A:
  $Object
  0
  5
  0 1 "Object$equals$Object"
  0 2 "Object$hashCode"
  0 3 "Object$toString"
  $A$f$A 2 "A$f$A"
  $A$f$Integer 2 "A$f$Integer"
B:
  $A
  0
  6
  0 1 "Object$equals$Object"
  0 2 "Object$hashCode"
  0 3 "Object$toString"
  $A$f$A 2 "A$f$A"
  $A$f$Integer 2 "A$f$Integer"
  $B$f$Integer$Integer 3 "B$f$Integer$Integer"
mainBlock:
# BlockStatementNode
# LocalVarDecStatementNode
  aconst_null
  astore 0
# LocalVarDecStatementNode
  aconst_null
  astore 1
# ExpressionStatementNode
# AssignmentOperatorNode
# VarNode
# ClassInstanceCreationExpressionNode
  new $A
  dup
  invokespecial $A_constructor 1 1
  astore 0
# ExpressionStatementNode
# AssignmentOperatorNode
# VarNode
# ClassInstanceCreationExpressionNode
  new $B
  dup
  invokespecial $B_constructor 1 1
  astore 1
# OutStatementNode
# MethodInvocationExpressionNode
# MethodInvocationExpressionNode
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 1
# IntegerLiteralNode
  newint 19
  invokevirtual 4 2
# IntegerLiteralNode
  newint 41
  invokevirtual 4 2
  invokevirtual 2 1
  out
# OutStatementNode
# StringLiteralNode
  newstr "
"
  out
# OutStatementNode
# MethodInvocationExpressionNode
# MethodInvocationExpressionNode
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 1
# DerefNode
# VarNode
  aload 0
  invokevirtual 3 2
# IntegerLiteralNode
  newint 40
  invokevirtual 4 2
  invokevirtual 2 1
  out
# OutStatementNode
# StringLiteralNode
  newstr "
"
  out
# OutStatementNode
# MethodInvocationExpressionNode
# MethodInvocationExpressionNode
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 1
# DerefNode
# VarNode
  aload 1
  invokevirtual 3 2
# IntegerLiteralNode
  newint 40
  invokevirtual 4 2
  invokevirtual 2 1
  out
# OutStatementNode
# StringLiteralNode
  newstr "
"
  out
# OutStatementNode
# MethodInvocationExpressionNode
# MethodInvocationExpressionNode
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 1
# IntegerLiteralNode
  newint 7
# IntegerLiteralNode
  newint 8
  invokevirtual 5 3
# IntegerLiteralNode
  newint 27
  invokevirtual 4 2
  invokevirtual 2 1
  out
# OutStatementNode
# StringLiteralNode
  newstr "
"
  out
  newint 0
mainBlock$.exit:
  areturn
A_constructor:
  aload 0
  invokenative 0
A_constructor$.exit:
  return
# ClassMethodNode
A$f$Integer:
# BlockStatementNode
# ReturnStatementNode
# IntegerLiteralNode
  newint 1
  goto $A$f$Integer$.exit
  aconst_null
A$f$Integer$.exit:
  areturn
# ClassMethodNode
A$f$A:
# BlockStatementNode
# ReturnStatementNode
# IntegerLiteralNode
  newint 2
  goto $A$f$A$.exit
  aconst_null
A$f$A$.exit:
  areturn
B_constructor:
  aload 0
  invokespecial $A_constructor 1 1
B_constructor$.exit:
  return
# ClassMethodNode
B$f$Integer$Integer:
# BlockStatementNode
# ReturnStatementNode
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 1
# DerefNode
# VarNode
  aload 2
  invokevirtual 4 2
  goto $B$f$Integer$Integer$.exit
  aconst_null
B$f$Integer$Integer$.exit:
  areturn
