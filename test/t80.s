# ClassDecNode
# ClassDecNode
# ClassDecNode
# ClassDecNode
# MainBlockNode
$mainBlock 4 
8
$Object "Object"
$Integer "Integer"
$String "String"
$Table "Table"
$AA "AA"
$A "A"
$B "B"
$C "C"
Object:
  0
  0
  3
  0 1 "Object$equals$Object"
  0 2 "Object$hashCode"
  0 3 "Object$toString"
Integer:
  $Object
  0
  19
  0 22 "Integer$equals$Object"
  0 23 "Integer$hashCode"
  0 24 "Integer$toString"
  0 6 "Integer$add$Integer"
  0 14 "Integer$operator+$Integer"
  0 7 "Integer$subtract$Integer"
  0 15 "Integer$operator-$Integer"
  0 8 "Integer$multiply$Integer"
  0 16 "Integer$operator*$Integer"
  0 9 "Integer$divide$Integer"
  0 17 "Integer$operator/$Integer"
  0 10 "Integer$greaterThan$Integer"
  0 18 "Integer$operator>$Integer"
  0 11 "Integer$lessThan$Integer"
  0 19 "Integer$operator<$Integer"
  0 12 "Integer$not"
  0 20 "Integer$operator!"
  0 13 "Integer$minus"
  0 21 "Integer$operator-"
String:
  $Object
  0
  10
  0 34 "String$equals$Object"
  0 33 "String$hashCode"
  0 35 "String$toString"
  0 26 "String$length"
  0 27 "String$substr$Integer$Integer"
  0 29 "String$concat$String"
  0 28 "String$toInteger"
  0 30 "String$operator+$String"
  0 31 "String$operator>$String"
  0 32 "String$operator<$String"
Table:
  $Object
  0
  8
  0 1 "Object$equals$Object"
  0 2 "Object$hashCode"
  0 3 "Object$toString"
  0 38 "Table$get$Object"
  0 39 "Table$put$Object$Object"
  0 40 "Table$remove$Object"
  0 41 "Table$firstKey"
  0 42 "Table$nextKey"
AA:
  $Object
  0
  7
  0 1 "Object$equals$Object"
  0 2 "Object$hashCode"
  0 3 "Object$toString"
  $AA$f3 1 "AA$f3"
  $AA$f2 1 "AA$f2"
  $AA$f1 1 "AA$f1"
  $AA$f$Integer$Integer$Integer 4 "AA$f$Integer$Integer$Integer"
A:
  $Object
  0
  3
  0 1 "Object$equals$Object"
  0 2 "Object$hashCode"
  0 3 "Object$toString"
B:
  $Object
  0
  3
  0 1 "Object$equals$Object"
  0 2 "Object$hashCode"
  0 3 "Object$toString"
C:
  $Object
  0
  3
  0 1 "Object$equals$Object"
  0 2 "Object$hashCode"
  0 3 "Object$toString"
mainBlock:
# BlockStatementNode
# LocalVarDecStatementNode
  aconst_null
  astore 0
# ExpressionStatementNode
# AssignmentOperatorNode
# VarNode
# ClassInstanceCreationExpressionNode
  new $AA
  dup
  invokespecial $AA_constructor 1 1
  astore 0
# ExpressionStatementNode
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 0
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 0
  invokevirtual 5 1
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 0
  invokevirtual 4 1
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 0
  invokevirtual 3 1
  invokevirtual 6 4
  pop
# LocalVarDecStatementNode
  aconst_null
  astore 1
# LocalVarDecStatementNode
  aconst_null
  astore 2
# LocalVarDecStatementNode
  aconst_null
  astore 3
# ExpressionStatementNode
# AssignmentOperatorNode
# VarNode
# ClassInstanceCreationExpressionNode
  new $A
  dup
# IntegerLiteralNode
  newint 42
  invokespecial $A_constructor$Integer 2 2
  astore 1
# ExpressionStatementNode
# AssignmentOperatorNode
# VarNode
# ClassInstanceCreationExpressionNode
  new $B
  dup
# IntegerLiteralNode
  newint 4
# IntegerLiteralNode
  newint 2
  invokespecial $B_constructor$Integer$Integer 3 3
  astore 2
# ExpressionStatementNode
# AssignmentOperatorNode
# VarNode
# ClassInstanceCreationExpressionNode
  new $C
  dup
# IntegerLiteralNode
  newint 5
# IntegerLiteralNode
  newint 8
# IntegerLiteralNode
  newint 1
  invokespecial $C_constructor$Integer$Integer$Integer 4 4
  astore 3
  newint 0
mainBlock$.exit:
  areturn
AA_constructor:
  aload 0
  invokenative 0
AA_constructor$.exit:
  return
# ClassMethodNode
AA$f$Integer$Integer$Integer:
# BlockStatementNode
# OutStatementNode
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 1
  invokevirtual 2 1
  out
# OutStatementNode
# StringLiteralNode
  newstr "
"
  out
# OutStatementNode
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 2
  invokevirtual 2 1
  out
# OutStatementNode
# StringLiteralNode
  newstr "
"
  out
# OutStatementNode
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 3
  invokevirtual 2 1
  out
# OutStatementNode
# StringLiteralNode
  newstr "
"
  out
  aconst_null
AA$f$Integer$Integer$Integer$.exit:
  areturn
# ClassMethodNode
AA$f1:
# BlockStatementNode
# OutStatementNode
# MethodInvocationExpressionNode
# IntegerLiteralNode
  newint 1
  invokevirtual 2 1
  out
# OutStatementNode
# StringLiteralNode
  newstr "
"
  out
# ReturnStatementNode
# IntegerLiteralNode
  newint 1
  goto $AA$f1$.exit
  aconst_null
AA$f1$.exit:
  areturn
# ClassMethodNode
AA$f2:
# BlockStatementNode
# OutStatementNode
# MethodInvocationExpressionNode
# IntegerLiteralNode
  newint 2
  invokevirtual 2 1
  out
# OutStatementNode
# StringLiteralNode
  newstr "
"
  out
# ReturnStatementNode
# IntegerLiteralNode
  newint 2
  goto $AA$f2$.exit
  aconst_null
AA$f2$.exit:
  areturn
# ClassMethodNode
AA$f3:
# BlockStatementNode
# OutStatementNode
# MethodInvocationExpressionNode
# IntegerLiteralNode
  newint 3
  invokevirtual 2 1
  out
# OutStatementNode
# StringLiteralNode
  newstr "
"
  out
# ReturnStatementNode
# IntegerLiteralNode
  newint 3
  goto $AA$f3$.exit
  aconst_null
AA$f3$.exit:
  areturn
# ClassConstructorNode
A_constructor$Integer:
# BlockStatementNode
# ConstructorInvocationStatementNode
# SuperExpressionNode
  aload 0
  invokenative 0
# OutStatementNode
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 1
  invokevirtual 2 1
  out
# OutStatementNode
# StringLiteralNode
  newstr "
"
  out
A_constructor$Integer$.exit:
  return
# ClassConstructorNode
B_constructor$Integer$Integer:
# BlockStatementNode
# ConstructorInvocationStatementNode
# SuperExpressionNode
  aload 0
  invokenative 0
# OutStatementNode
# MethodInvocationExpressionNode
# MethodInvocationExpressionNode
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 1
# IntegerLiteralNode
  newint 10
  invokevirtual 8 2
# DerefNode
# VarNode
  aload 2
  invokevirtual 4 2
  invokevirtual 2 1
  out
# OutStatementNode
# StringLiteralNode
  newstr "
"
  out
B_constructor$Integer$Integer$.exit:
  return
# ClassConstructorNode
C_constructor$Integer$Integer$Integer:
# BlockStatementNode
# ConstructorInvocationStatementNode
# SuperExpressionNode
  aload 0
  invokenative 0
# OutStatementNode
# MethodInvocationExpressionNode
# MethodInvocationExpressionNode
# MethodInvocationExpressionNode
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 1
# IntegerLiteralNode
  newint 5
  invokevirtual 8 2
# MethodInvocationExpressionNode
# DerefNode
# VarNode
  aload 2
# IntegerLiteralNode
  newint 2
  invokevirtual 8 2
  invokevirtual 4 2
# DerefNode
# VarNode
  aload 3
  invokevirtual 4 2
  invokevirtual 2 1
  out
# OutStatementNode
# StringLiteralNode
  newstr "
"
  out
C_constructor$Integer$Integer$Integer$.exit:
  return
