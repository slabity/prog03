// test 80% level of Phase 4

class AA {
  Integer f(Integer a, Integer b, Integer c)
  {
    out a; out newline;
    out b; out newline;
    out c; out newline;
  }
  Integer f1() { out 1; out newline; return 1; }
  Integer f2() { out 2; out newline; return 2; }
  Integer f3() { out 3; out newline; return 3; }
}

class A {
  A(Integer x) { out x; out newline; }
} 

class B {
  B(Integer x, Integer y) { out x * 10 + y; out newline; }
}   
      
class C {
  C(Integer x, Integer y, Integer z) { out x * 5 + y * 2 + z; out newline; }
} 
      
Integer main()
{
  AA aa;

  aa = new AA();

  aa.f(aa.f1(), aa.f2(), aa.f3());

  A a;
  B b;
  C c;

  a = new A(42);

  b = new B(4, 2);

  c = new C(5, 8, 1);
}

